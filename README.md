#pymad8#

## Authors ##

M. Deniaud
S. Boogert
L. Nevay

## Setup ##

From within the pymad8 root directory:

$ make install

or for development:

$ make develop
